from typing import Any, List, Mapping, Optional
import requests
from os import environ
from dotenv import load_dotenv

from langchain_core.callbacks.manager import CallbackManagerForLLMRun
from langchain_core.language_models.llms import LLM

class MitraLLM(LLM):
    TOKEN:str = None
    URL:str   = None
    def __init__(self, token:str=None, accessapi:str=None, **kwargs):
        super().__init__()
        self.setting_token(token, accessapi)
    
    def setting_token(self, token:str=None, accessapi:str=None):
        # Attempt to get from the env
        load_dotenv()
        env = { item: environ[item] for item in ["TOKEN", "ACCESSAPI"] if item in environ }
        # Add the token and url from class definition
        if token:     env["TOKEN"] = token
        if accessapi: env["ACCESSAPI"] = accessapi

        if "TOKEN" not in env:
            raise Exception("TOKEN not found in the environment")
        self.TOKEN: str  = env["TOKEN"]
        self.URL: str    = env.get("ACCESSAPI", "https://mitrallm.mitrarobot.com")

    @property
    def _llm_type(self) -> str:
        return "mitra"

    def _call(
        self,
        prompt: str,
        stop: Optional[List[str]] = None,
        run_manager: Optional[CallbackManagerForLLMRun] = None,
        **kwargs: Any,
    ) -> str:

        headers = {
            'Authorization': f'Bearer {self.TOKEN}',
        }

        context = [
            {
                "role": "user",
                "content": prompt,
            }
        ]
        json_data = {
            'messages': context,
        }
        response = requests.post(f'{self.URL}/chatgpt/', json=json_data, headers=headers)
        response.raise_for_status()
        response_json = response.json()
        openai_response = response_json.get("openai_response")

        content = openai_response.get("choices")[0].get("message").get("content")
        return content

    @property
    def _identifying_params(self) -> Mapping[str, Any]:
        """Get the identifying parameters."""
        return {"URL": self.URL}
